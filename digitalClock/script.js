const hoursElement=document.getElementById("hours");
const minutesElement=document.getElementById("minutes");
const secondsElement=document.getElementById("seconds");
const ampmElement=document.getElementById("ampm")

function displayTime() {
    let noOfhours= new Date().getHours();
    let noOfminutes= new Date().getMinutes();
    let noOfseconds = new Date().getSeconds();
    let ampm = "AM";
  
    if (noOfhours > 12) {
        noOfhours= noOfhours- 12;
      ampm = "PM";
    }  
    hoursElement.innerText =noOfhours;
    minutesElement.innerText=noOfminutes;
    secondsElement.innerText=noOfseconds ;
    ampmElement.innerText=ampm;
    setTimeout(() => {
      displayTime();
    }, 1000);
  }
  
displayTime();


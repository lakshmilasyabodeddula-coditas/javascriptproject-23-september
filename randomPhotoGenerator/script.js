const imageContainerElement=document.querySelector(".image-container");
const loadButtonElement=document.querySelector(".button");

loadButtonElement.addEventListener("click",()=>{
    totalNumberOfImages=10
    addNewImages();
})
function addNewImages()
{
    for(let idx=0;idx<=totalNumberOfImages;idx++)
    {
        const newImageElement = document.createElement("img");
        newImageElement.src = `https://picsum.photos/300?random=${Math.floor(
      Math.random() * 2000
    )}`;
    imageContainerElement.appendChild(newImageElement);
    }
}
const nextElement = document.querySelector(".next");
const previousElement = document.querySelector(".prev");
const imagesElement = document.querySelectorAll("img");
const imageContainerElement = document.querySelector(".image-container");
let currentImage = 1;

let timeout;

nextElement.addEventListener("click", () => {
  currentImage++;
  clearTimeout(timeout);
  updateImage();
});

previousElement.addEventListener("click", () => {
  currentImage--;
  clearTimeout(timeout);
  updateImage();
});

updateImage();

function updateImage() {
  if (currentImage > imagesElement.length) {
    currentImage = 1;
  } else if (currentImage < 1) {
    currentImage = imagesElement.length;
  }
  imageContainerElement.style.transform = `translateX(-${(currentImage - 1) * 500}px)`;
  timeout = setTimeout(() => {
    currentImage++;
    updateImg();
  }, 3000);
}